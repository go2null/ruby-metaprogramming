#TODO: Refactor output group by Method instead of by Object

# Prints hierarchy, methods and instance variables of passed in object
module MyIntrospector
  def self.introspect_my(object = self, label = '')
    label = object.to_s if label.empty?
    objects = {}
    objects["#{label}.singleton_class"] = object.singleton_class
    objects[label]                      = object
    objects["#{label}.new"]             = object.respond_to?(:new) ? object.new : nil
    objects.each do |lbl, obj|
      puts lbl
      #obj ? IntrospectMy.new(obj).__send__(:introspect_my_object) : puts
      IntrospectMy.new(obj).__send__(:introspect_my_object) if obj
    end
  end

  # Worker class
  class IntrospectMy
    private

    attr_reader :my_object

    def initialize(object)
      @my_object = object
    end

    def introspect_my_object
      meths = []
      #[/class$/, /ancestors/].each do |regex|
      #  meths.concat my_methods(regex)
      #end
      #[/modules$/, /constants/, /methods$/, /variables$/].each do |regex|
      [/constants/, /methods$/, /variables$/].each do |regex|
        #meths.concat my_methods(my_object, meth).flat_map { |m| [{m => /my/}, m] }
        meths.concat my_methods(regex).map { |m| {m => /my/} }
      end
      meths.each { |h| my_values h }
      #puts
    end

    def my_methods(regex)
      my_object.methods.select { |m| m.to_s =~ regex }
    end

    def my_values(meth, regex = nil)
      meth, regex = meth.first if meth.is_a?(Hash)
      return unless my_object.respond_to?(meth)
      value = my_object.__send__(meth)
      if value.is_a?(Array)
        value.keep_if { |m| m.to_s.downcase =~ regex } if regex
        value = nil if value.empty?
      end
      puts "  #{meth}: #{value}" if value
    end
  end
end

module MyModule
  MY_MODULE = :my_module
  def self.my_self_method; MyIntrospector.introspect_my self; end
  def my_method;           MyIntrospector.introspect_my self; end
end

class MyModuleWithIncludedModule
  include MyModule
  MY_MODULE_WITH_INCLUDED_MODULE = :my_module_with_included_module
end

class MyModuleWithExtendedModule
  extend MyModule
  MY_MODULE_WITH_EXTENDED_MODULE = :my_module_with_extended_module
end

class MyModuleWithSelfIncludedModule
  class << self
    include MyModule
    MY_MODULE_WITH_SELF_INCLUDED_MODULE = :my_module_with_self_included_module
  end
end

class MyModuleWithSelfExtendedModule
  class << self
    extend MyModule
    MY_MODULE_WITH_SELF_EXTENDED_MODULE = :my_module_with_self_extended_module
  end
end

class MyClassWithIncludedModule
  include MyModule
  MY_CLASS_WITH_INCLUDED_MODULE = :my_class_with_included_module
end

class MyClassWithExtendedModule
  extend MyModule
  MY_CLASS_WITH_EXTENDED_MODULE = :my_class_with_extended_module
end

class MyClassWithSelfIncludedModule
  class << self
    include MyModule
    MY_CLASS_WITH_SELF_INCLUDED_MODULE = :my_class_with_self_included_module
  end
end

class MyClassWithSelfExtendedModule
  class << self
    extend MyModule
    MY_CLASS_WITH_SELF_EXTENDED_MODULE = :my_class_with_self_extended_module
  end
end

def runner
  [ MyModule,
    MyModuleWithIncludedModule,
     MyClassWithIncludedModule,
    MyModuleWithExtendedModule,
     MyClassWithExtendedModule,
    MyModuleWithSelfIncludedModule,
     MyClassWithSelfIncludedModule,
    MyModuleWithSelfExtendedModule,
     MyClassWithSelfExtendedModule
  ].each do |object|
    puts "##{object}.my_self_method"
    object.my_self_method if object.respond_to?(:my_self_method)
    puts "##{object}.my_method"
    object.my_method      if object.respond_to?(:my_method)
  end
end

runner
#MyIntrospector.introspect_my self
#MyIntrospector.introspect_my
#MyIntrospector.introspect_my MyModule
#MyIntrospector.introspect_my MyClassWithIncludedModule
#MyIntrospector.introspect_my MyClassWithSelfIncludedModule
#MyIntrospector.introspect_my MyClassWithExtendedModule
#MyIntrospector.introspect_my MyClassWithSelfExtendedModule
