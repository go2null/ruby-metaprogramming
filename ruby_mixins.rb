require 'pry-doc'

module AModule
  A_MODULE_CONSTANT = 'PASS    A_MODULE_CONSTANT'.freeze

  def self.a_module_method(scope)
    puts "#{scope} #{self}.#{__method__}"
  end

  def a_module_instance_method(scope)
    puts "#{scope} #{self}.#{__method__}"
  end

  def self.a_protected_module_method_wrapper(scope)
    puts "#{scope} #{self}.#{__method__}"
    a_protected_module_instance_method(scope)
  end

  def self.a_private_module_method_wrapper(scope)
    puts "#{scope} #{self}.#{__method__}"
    a_private_module_instance_method(scope)
  end

  protected
  
  def a_protected_module_instance_method(scope)
    puts "#{scope} #{self}.#{__method__}"
  end

  private 
  
  def a_private_module_instance_method(scope)
    puts "#{scope} #{self}.#{__method__}"
  end
end

class IncludeA
  include AModule
end

class ExtendA
  extend AModule
end

class AClass
  A_CLASS_CONSTANT = 'PASS    A_CLASS_CONSTANT'.freeze

  def self.a_class_method(scope)
    puts "#{scope} #{self}.#{__method__}"
  end

  def self.a_private_class_method_wrapper(scope)
    puts "#{scope} #{self}.#{__method__}"
    a_private_class_method
  end

  private_class_method def self.a_private_class_method(scope)
    puts "#{scope} #{self}.#{__method__}"
  end

  def a_class_instance_method(scope)
    puts "#{scope} #{self}.#{__method__}"
  end

  def a_protected_class_instance_method_wrapper(scope)
    puts "#{scope} #{self}.#{__method__}"
    a_protected_class_instance_method(scope)
  end

  def a_private_class_instance_method_wrapper(scope)
    puts "#{scope} #{self}.#{__method__}"
    a_private_class_instance_metho(scope)
  end

  protected

  def a_protected_class_instance_method(scope)
    puts "#{scope} #{self}.#{__method__}"
  end

  private

  def a_private_class_instance_method(scope)
    puts "#{scope} #{self}.#{__method__}"
  end

end

class ASubClass < AClass
end

objects = [AModule, IncludeA, IncludeA.new, ExtendA, ExtendA.new, 
           AClass, AClass.new, ASubClass, ASubClass.new]
methods = [:constants,
           :public_methods,       :public_instance_methods,
           :protected_methods, :protected_instance_methods,
           :private_methods,     :private_instance_methods]

objects.each do |obj|
  puts obj
  methods.each do |met|
    next unless obj.respond_to?(met)

    mets = obj.send(met).select { |m| m.to_s.downcase.start_with?('a_') }
    puts "#{met} #{mets}" unless mets.empty?
  end
end
